import { useState, useEffect } from 'react'
import { asyncStorage } from '../async-storage'

export function AsyncStorageValueProvider(props: {
  key: string
  children: (value: string) => any
}) {
  const { key } = props
  const [value, setValue] = useState('__loading__')
  useEffect(() => {
    asyncStorage
      .get(key)
      // .then(value => setState(value))
      .then(setValue)
  }, [key])
  return <>{value === '__loading__' ? null : props.children(value)}</>
}
