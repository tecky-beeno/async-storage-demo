import { useEffect, useState } from 'react'
import { asyncStorage } from './async-storage'
import { AsyncStorageValueProvider } from './components/AsyncStorageValueProvider'

export function Demo() {
  const [state, setState] = useState('__loading__')
  useEffect(() => {
    asyncStorage
      .get('token')
      // .then(value => setState(value))
      .then(setState)
  }, [])
  return <div></div>
}

export function Demo2() {
  return (
    <div>
      <header>Profile</header>
      <AsyncStorageValueProvider key="token">
        {(token: string) => (
          <>{token ? 'user data ...' : 'you have not login'}</>
        )}
      </AsyncStorageValueProvider>
    </div>
  )
}
